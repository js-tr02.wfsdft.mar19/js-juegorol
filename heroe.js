export class Heroe {
    constructor(nombre, profesion, raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa, url_imagen) {
        this._nombre = nombre;
        this.profesion = profesion;
        this.raza = raza;
        this.edad = edad;
        this.altura = altura;
        this.peso = peso;
        this.sexo = sexo;
        this.experiencia = experiencia;
        this.pv = pv;
        this.ataque = ataque;
        this.defensa = defensa;
        this.url_imagen = url_imagen;
    }
    get nombre () {
        return this._nombre;
    }
}
export class Mago extends Heroe {
    constructor (nombre, profesion, raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa,inteligencia, hechizos, url_imagen ) {
        super (nombre, profesion, raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa, url_imagen);
        this.inteligencia = inteligencia;
        this.hechizos = hechizos;
        }
        lanzarHechizo (indice_hechizos) {
            this.hechizos[indice_hechizos];
        }    
}

export class Ladron extends Heroe {
    constructor (nombre, profesion,  raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa, sigilo, astucia, url_imagen ) {
        super (nombre, profesion, raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa, url_imagen);
        this.sigilo = sigilo;
        this.astucia = astucia;
        }
    roba (sigilo) {
        if (this.sigilo > sigilo)
        {
            console.log ("El ladrón te roba");
        }
    } 
}

export class Guerrero extends Heroe {
    constructor (nombre, profesion, raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa, url_imagen ) {
        super (nombre, profesion, raza, edad, altura, peso, sexo, experiencia, pv, ataque, defensa, url_imagen);
        }
}