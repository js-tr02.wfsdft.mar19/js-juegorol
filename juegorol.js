import {Mago,Ladron,Guerrero} from './heroe.js';

var hechizos_nigromante = [
    { 
      "hechizo"  :  "Bola de Fuego: +90 a ataque", 
      "suma_ataque"   :  90,
      "suma_defensa"   :  0
    },
    { 
        "hechizo"  :  "Inmovilizar: +100 a defensa", 
        "suma_ataque"   :  0,
        "suma_defensa"   :  100
    },
    {
        "hechizo"  :  "Axfisia: +90 a ataque", 
        "suma_ataque"   :  90,
        "suma_defensa"   :  0
    }
];

var estado_anterior = [
{
    "ataque": 0,
    "defensa": 0
}];

// Creamos los objetos Heroes
var nigromante = new Mago ('Saruman','Mago','Humano',150,195,85,'Hombre',150800,90,150,80,70,hechizos_nigromante,'img/mago.jpg');
var thror = new Guerrero ('Thror','Guerrero','enano',50,135,95,'Hombre',6800,190,210,140,'img/enano.jpg');
var robin = new Ladron ('Robin','Ladrón','Humano',28,175,65,'Hombre',20500,114,140,160,120,160,'img/ladron.jpg');

var mis_personajes = [];
mis_personajes.push (nigromante);
mis_personajes.push (thror);
mis_personajes.push (robin);

// Selecciono el template y el target
var template = document.querySelector("#item");
var target = document.querySelector("#colItems");

mis_personajes.forEach(pinta_personajes);

// Función para pintar los personajes y las opciones de ataque de los personajes
function pinta_personajes (personaje,i,arraymispersonajes)
{
    var nodo_clonado = template.content.cloneNode(true);

    var div = nodo_clonado.querySelector("div");
    div.id = "elemDiv" + i;
    var h4 = nodo_clonado.querySelector("h4");
    h4.innerHTML = mis_personajes[i].nombre;
    let img = nodo_clonado.querySelector("img");
    img.src = mis_personajes[i].url_imagen;

    let h5_profesion = nodo_clonado.querySelector("h5");
    h5_profesion.innerHTML = mis_personajes[i].profesion;

    var p_personaje_info = nodo_clonado.querySelector("p");
    p_personaje_info.innerHTML = "Raza: " + mis_personajes[i].raza 
    + "</br>Edad: " + mis_personajes[i].edad
    + "</br>Altura: " + mis_personajes[i].altura
    + "</br>Peso: " + mis_personajes[i].peso
    + "</br>Sexo: " + mis_personajes[i].sexo
    + "</br>Experiencia: " + mis_personajes[i].experiencia
    //+ "</br>Hechizos: " +  consulta_hechizos(i)
    + "</br>Sigilo: " + consulta_sigilo(i)
    + "</br>Astucia: " + consulta_astucia(i)
    + "</br>Ataque: <span class=\"ataque\">" + mis_personajes[i].ataque + "</span>"
    + "</br>Defensa: <span class=\"defensa\">" + mis_personajes[i].defensa + "</span>"
    + "</br>Puntos de Vida: <span class=\"puntos_vida\">" + mis_personajes[i].pv + "</span></br>";

    var selectAtaque = document.createElement("select");
    selectAtaque.className = "ataque form-control";

    for (var x = 0; x < mis_personajes.length; x++)
    {
        var optionSelect = document.createElement("option");
        optionSelect.innerHTML = "Atacar a " + mis_personajes[x].nombre;
        optionSelect.value = x;
        selectAtaque.appendChild(optionSelect);
    }
    if (mis_personajes[i].profesion == 'Mago')
    {
        p_personaje_info.appendChild(consulta_hechizos(i));
        let selectHechizo = p_personaje_info.querySelector("select.hechizos");
        selectHechizo.addEventListener('change', modifica_ataque);
    }  
    p_personaje_info.appendChild(selectAtaque);
    
    var boton_add = nodo_clonado.querySelector("button");
    boton_add.id = i;
    boton_add.addEventListener('click', atacar);
    target.appendChild(nodo_clonado);
}

// Función atacar: se evalua el ataque y defensa de los personajes y se resta de los puntos de vida.
function atacar (event) {
    let id_atacante = event.target.id;
    let ficha_personaje = this.parentNode;
    let id_atacado = ficha_personaje.querySelector("select.ataque").value;
    if (mis_personajes[id_atacante].profesion == 'Mago')
    {
        let id_hechizo = ficha_personaje.querySelector("select.hechizos").value;
    }
   
    let vida = mis_personajes[id_atacado].pv - (mis_personajes[id_atacante].ataque - mis_personajes[id_atacado].defensa);
    let porcentaje_vida = vida / mis_personajes[id_atacado].pv * 100;
    mis_personajes[id_atacado].pv = vida;

    let ficha_atacado = document.querySelector("#elemDiv"+id_atacado);
    let progress_atacado = ficha_atacado.querySelector("div.progress-bar");
    let vida_atacado = ficha_atacado.querySelector("span.puntos_vida");
    vida_atacado.innerHTML = vida;
    porcentaje_vida = porcentaje_vida < 0 ? 0 : porcentaje_vida;

    if (porcentaje_vida < 50)
    {
        progress_atacado.classList.remove("bg-success");
        progress_atacado.classList.add("bg-danger");
    }

    if (porcentaje_vida == 0)
    {
        ficha_atacado.style.backgroundColor = "#222222";
        let select_atacado = ficha_atacado.querySelector("select.ataque");
        let btn_atacado = ficha_atacado.querySelector("button");
        select_atacado.remove();
        btn_atacado.remove();
    }
    progress_atacado.style.width = porcentaje_vida + "%";
}

// Esta función genera el select de hechizos para el mago
function consulta_hechizos (ind)
{   
    if (mis_personajes[ind].profesion == 'Mago')
    {
        var selectHechizo = document.createElement("select");
        for (var x = 0; x < hechizos_nigromante.length; x++)
        {
        var optionSelect = document.createElement("option");
        optionSelect.innerHTML = hechizos_nigromante[x].hechizo;
        optionSelect.value = x;
        selectHechizo.appendChild(optionSelect);
        }
        selectHechizo.className = "hechizos form-control";
        return selectHechizo;
    }
    else 
    {
        return "No tiene";
    }
}

// Función para modificar parametros de ataque del mago en función de los hechizos
function modifica_ataque (event)
{
    let ficha_personaje = this.parentNode.parentNode;
    let id_atacante = ficha_personaje.querySelector("button").id;
    let hechizo_id = ficha_personaje.querySelector("select.hechizos").value;
    mis_personajes[id_atacante].ataque = mis_personajes[id_atacante].ataque + hechizos_nigromante[hechizo_id].suma_ataque - estado_anterior[0].ataque;
    mis_personajes[id_atacante].defensa = mis_personajes[id_atacante].defensa + hechizos_nigromante[hechizo_id].suma_defensa - estado_anterior[0].defensa;
    ficha_personaje.querySelector("span.ataque").innerHTML = mis_personajes[id_atacante].ataque;
    ficha_personaje.querySelector("span.defensa").innerHTML = mis_personajes[id_atacante].defensa;
    // Hay que actualizar el ataque
    estado_anterior[0].ataque = hechizos_nigromante[hechizo_id].suma_ataque;
    estado_anterior[0].defensa = hechizos_nigromante[hechizo_id].suma_defensa;
}

function consulta_sigilo (ind)
{
    return typeof(mis_personajes[ind].sigilo) != 'undefined' ? mis_personajes[ind].sigilo : "No tiene";
}

function consulta_astucia (ind)
{
    return typeof(mis_personajes[ind].astucia) != 'undefined' ? mis_personajes[ind].astucia : "No tiene";
}